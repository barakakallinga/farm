﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;
using SupportActionBar = Android.Support.V7.App.ActionBar;
using Android.Views;
using Android.Content;
using farm.fragment;
using System.Threading.Tasks;
using Steelkiwi.Com.Library;

namespace farm
{
    [Activity(Label = "FARM", MainLauncher = true, Theme = "@style/Theme.DesignDemo")]
    public class MainActivity : AppCompatActivity
    {
        public static string LanguageSetting;
        DotsLoaderView dotsLoaderView;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);


            LanguageSetting = retrieveset();
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);
            SupportToolbar toolBar = FindViewById<SupportToolbar>(Resource.Id.toolBar);
            toolBar.InflateMenu(Resource.Menu.home);
            SetSupportActionBar(toolBar);
            SupportActionBar ab = SupportActionBar;
            // var p =  GetHomelistAsync();

            dotsLoaderView = FindViewById<DotsLoaderView>(Resource.Id.dotsLoaderView);
            DemoDownload();
           


            Android.Support.V4.App.FragmentTransaction transaction1 = SupportFragmentManager.BeginTransaction();

            home one = new home();
            transaction1.Replace(Resource.Id.container, one);
            transaction1.AddToBackStack(null);
            transaction1.Commit();
            

        }

        private void DemoDownload()
        {
            dotsLoaderView.Show();
            Task.Delay(5000)
                .ContinueWith(t =>
                {
                    dotsLoaderView.Hide();
                });
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {

            MenuInflater.Inflate(Resource.Menu.home, menu);

            IMenuItem menuItemSwahili = menu.FindItem(Resource.Id.nav_swahili);

            IMenuItem menuItemEnglish = menu.FindItem(Resource.Id.nav_english);

            if (LanguageSetting == "swahili")
            {
                menuItemSwahili.SetChecked(true);
            }
            else if (LanguageSetting == "english")
            {
                menuItemEnglish.SetChecked(true);
            }


            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {


            Intent i = BaseContext.PackageManager.GetLaunchIntentForPackage(BaseContext.PackageName);
            switch (item.ItemId)
            {
                case Resource.Id.nav_swahili:
                   
                        LanguageSetting = "swahili";

                        item.SetChecked(true);
                    saveset("swahili");
                  

                    i.AddFlags(ActivityFlags.ClearTop);
                    StartActivity(i);


                    return true;
                case Resource.Id.nav_english:
                   
                        LanguageSetting = "english";

                        item.SetChecked(true);
                    saveset("english");
                

                    i.AddFlags(ActivityFlags.ClearTop);
                    StartActivity(i);

                    return true;

                default:
                    return base.OnOptionsItemSelected(item);
            }
        }



        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (LanguageSetting == "swahili")
            {
                saveset("swahili");
            }
            else if (LanguageSetting == "english")
            {
                saveset("english");
            }


        }

        protected void saveset(string option)
        {

            //store
            var prefs = Application.Context.GetSharedPreferences("farm", FileCreationMode.Private);
            var prefEditor = prefs.Edit();
            prefEditor.PutString("Languagefarm", option);

            prefEditor.Commit();

        }

        protected string retrieveset()
        {
            //retreive 
            var prefs = Application.Context.GetSharedPreferences("farm", FileCreationMode.Private);
            var somePref = prefs.GetString("Languagefarm", null);

            return somePref;

        }



    }
}

