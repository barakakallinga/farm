﻿using Android.Content;
using Android.Content.Res;
using Android.Support.V7.Widget;
using Android.Util;
using Android.Views;
using Android.Widget;
using farm.Model;
using System.Collections.Generic;

namespace farm.Adapters
{
    public   class TreatmentRecyclerAdapter : RecyclerView.Adapter
    {



        private Context mContext;

        private readonly TypedValue mTypedValue = new TypedValue();
        Resources mResource;
        private int mBackground;
        public List<combinedList> t1List = new List<combinedList>();
        public List<Treatment> trList = new List<Treatment>();
      



        public TreatmentRecyclerAdapter(Context context, List<combinedList> tLists, Resources res)
        {
            context.Theme.ResolveAttribute(Resource.Attribute.selectableItemBackground, mTypedValue, true);
            mContext = context;

            mBackground = mTypedValue.ResourceId;
            t1List = tLists;
            mResource = res;

        }



        public override int ItemCount
        {
            get
            {
                return t1List.Count;
            }
        }

        public override int GetItemViewType(int position)
        {
            combinedList combinedList = t1List[position];
            return combinedList.mType;
        }




        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {

            switch(viewType)
            {
                case 0:
                    View v = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.treatmentItemHeader, parent, false);
                    v.SetBackgroundResource(mBackground);
                    return new TreatmentHeaderViewHolder(v);

                case 1:

                    View v1 = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.treatmentItem, parent, false);
                    v1.SetBackgroundResource(mBackground);
                    return new TreatmentViewHolder(v1);



            }

            return null;

           
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {

            combinedList combinedList = t1List[position];

            switch (combinedList.mType)
            {

                case 0:
                    var simpleHeadHolder = holder as TreatmentHeaderViewHolder;
                    simpleHeadHolder.mTxtViewHeader.Text = "Disease: " + t1List[position].Disease;
                    break;

                case 1:
                    var simpleHolder = holder as TreatmentViewHolder;
                    simpleHolder.mTxtView.Text = t1List[position].treatment;
                    break;

            }
         
            
        }
    }



    public class TreatmentViewHolder : RecyclerView.ViewHolder
    {
        public readonly CardView cv;
        public readonly View mView;
        public readonly TextView mTxtView;
        public readonly ImageView mImageView;
        public readonly CheckBox mCheckbox;




        public TreatmentViewHolder(View view) : base(view)
        {

            cv = view.FindViewById<CardView>(Resource.Id.treatment_card);
            mView = view;

            mTxtView = view.FindViewById<TextView>(Resource.Id.treatment_list_title);
            // mImageView = view.FindViewById<ImageView>(Resource.Id.cardview_image);
           



        }


    }

    public class TreatmentHeaderViewHolder : RecyclerView.ViewHolder
    {
        public readonly CardView cv;
        public readonly View mView;
        public readonly TextView mTxtViewHeader;
        




        public TreatmentHeaderViewHolder(View view) : base(view)
        {
           
            mView = view;

            mTxtViewHeader = view.FindViewById<TextView>(Resource.Id.TreatmenttitleTextView);
            // mImageView = view.FindViewById<ImageView>(Resource.Id.cardview_image);




        }


    }
}