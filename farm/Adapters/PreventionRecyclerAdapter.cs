﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Util;
using Android.Views;
using Android.Widget;
using farm.Model;

namespace farm.Adapters
{
   public  class PreventionRecyclerAdapter : RecyclerView.Adapter
    {

        private Context mContext;

        private readonly TypedValue mTypedValue = new TypedValue();
        Resources mResource;
        private int mBackground;
        public List<PreventionCombinedList> t1List = new List<PreventionCombinedList>();

        public PreventionRecyclerAdapter(Context context, List<PreventionCombinedList> tLists, Resources res)
        {
            context.Theme.ResolveAttribute(Resource.Attribute.selectableItemBackground, mTypedValue, true);
            mContext = context;

            mBackground = mTypedValue.ResourceId;
            t1List = tLists;
            mResource = res;

        }

        public override int ItemCount
        {
            get
            {
                return t1List.Count;
            }
        }


        public override int GetItemViewType(int position)
        {
            PreventionCombinedList combinedList = t1List[position];
            return combinedList.mType;
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {

            switch (viewType)
            {
                case 0:
                    View v = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.preventionItemHeader, parent, false);
                    v.SetBackgroundResource(mBackground);
                    return new PreventionHeaderViewHolder(v);

                case 1:
                    View v1 = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.preventionItem, parent, false);
                    v1.SetBackgroundResource(mBackground);
                    return new PreventionViewHolder(v1);
            }

            return null;


        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {

            PreventionCombinedList combinedList = t1List[position];

            switch (combinedList.mType)
            {

                case 0:
                    var simpleHeadHolder = holder as PreventionHeaderViewHolder;
                    simpleHeadHolder.mTxtViewHeader.Text = "Disease: " + t1List[position].Disease;
                    break;

                case 1:
                    var simpleHolder = holder as PreventionViewHolder;
                    simpleHolder.mTxtView.Text = t1List[position].prevention;
                    break;

            }


        }




    }

    public class PreventionViewHolder : RecyclerView.ViewHolder
    {
        public readonly CardView cv;
        public readonly View mView;
        public readonly TextView mTxtView;
       
       




        public PreventionViewHolder(View view) : base(view)
        {

            cv = view.FindViewById<CardView>(Resource.Id.advice_card_prevention);
            mView = view;

            mTxtView = view.FindViewById<TextView>(Resource.Id.prevention_list_title);
            // mImageView = view.FindViewById<ImageView>(Resource.Id.cardview_image);
        }


    }

    public class PreventionHeaderViewHolder : RecyclerView.ViewHolder
    {
        public readonly CardView cv;
        public readonly View mView;
        public readonly TextView mTxtViewHeader;





        public PreventionHeaderViewHolder(View view) : base(view)
        {

            mView = view;

            mTxtViewHeader = view.FindViewById<TextView>(Resource.Id.preventiontitleTextView);
            // mImageView = view.FindViewById<ImageView>(Resource.Id.cardview_image);


        }








    }
}