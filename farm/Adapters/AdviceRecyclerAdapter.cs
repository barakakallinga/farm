﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Util;
using Android.Views;
using Android.Widget;
using farm.Model;

namespace farm.Adapters
{
    public  class AdviceRecyclerAdapter : RecyclerView.Adapter
    {
        private Context mContext;

        private readonly TypedValue mTypedValue = new TypedValue();
        Resources mResource;
        private int mBackground;
        public List<AdviceCombinedList> t1List = new List<AdviceCombinedList>();

        public AdviceRecyclerAdapter(Context context, List<AdviceCombinedList> tLists, Resources res)
        {
            context.Theme.ResolveAttribute(Resource.Attribute.selectableItemBackground, mTypedValue, true);
            mContext = context;

            mBackground = mTypedValue.ResourceId;
            t1List = tLists;
            mResource = res;

        }

        public override int ItemCount
        {
            get
            {
                return t1List.Count;
            }
        }


        public override int GetItemViewType(int position)
        {
            AdviceCombinedList combinedList = t1List[position];
            return combinedList.mType;
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {

            switch (viewType)
            {
                case 0:
                    View v = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.adviceItemHeader, parent, false);
                    v.SetBackgroundResource(mBackground);
                    return new AdviceHeaderViewHolder(v);

                case 1:
                    View v1 = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.adviceItem, parent, false);
                    v1.SetBackgroundResource(mBackground);
                    return new AdviceViewHolder(v1);
            }

            return null;


        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {

            AdviceCombinedList combinedList = t1List[position];

            switch (combinedList.mType)
            {

                case 0:
                    var simpleHeadHolder = holder as AdviceHeaderViewHolder;
                    simpleHeadHolder.mTxtViewHeader.Text = "Disease: " + t1List[position].Disease;
                    break;

                case 1:
                    var simpleHolder = holder as AdviceViewHolder;
                    simpleHolder.mTxtView.Text = t1List[position].advice;
                    break;

            }


        }




    }

    public class AdviceViewHolder : RecyclerView.ViewHolder
    {
        public readonly CardView cv;
        public readonly View mView;
        public readonly TextView mTxtView;
        public readonly ImageView mImageView;
        public readonly CheckBox mCheckbox;




        public AdviceViewHolder(View view) : base(view)
        {

            cv = view.FindViewById<CardView>(Resource.Id.advice_card);
            mView = view;

            mTxtView = view.FindViewById<TextView>(Resource.Id.advice_list_title);
            // mImageView = view.FindViewById<ImageView>(Resource.Id.cardview_image);
        }


    }

    public class AdviceHeaderViewHolder : RecyclerView.ViewHolder
    {
        public readonly CardView cv;
        public readonly View mView;
        public readonly TextView mTxtViewHeader;





        public AdviceHeaderViewHolder(View view) : base(view)
        {

            mView = view;

            mTxtViewHeader = view.FindViewById<TextView>(Resource.Id.advicetitleTextView);
            // mImageView = view.FindViewById<ImageView>(Resource.Id.cardview_image);


        }


    }




}