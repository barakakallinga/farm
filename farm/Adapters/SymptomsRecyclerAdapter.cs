﻿using Android.Content;
using Android.Content.Res;
using Android.Support.V7.Widget;
using Android.Util;
using Android.Views;
using Android.Widget;
using farm.Model;
using Square.Picasso;
using System;
using System.Collections.Generic;

namespace farm.Adapters
{
    public  class SymptomsRecyclerAdapter : RecyclerView.Adapter
    {
    

        private Context mContext;

        private readonly TypedValue mTypedValue = new TypedValue();
        Resources mResource;
        private int mBackground;
        public List<Symptoms> s1List = new List<Symptoms>();
        public static List<string>  myList = new List<string>();

        public List<string> GetList()
        {
            return myList;
        }

        public  void  ClerList()
        {
            myList.Clear();
            
        }



        public SymptomsRecyclerAdapter(Context context, List<Symptoms> hLists, Resources res)
        {
            context.Theme.ResolveAttribute(Resource.Attribute.selectableItemBackground, mTypedValue, true);
            mContext = context;

            mBackground = mTypedValue.ResourceId;
            s1List = hLists;
            mResource = res;

        }



        public override int ItemCount
        {
            get
            {
                return s1List.Count;
            }
        }





        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View v = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.symptomItem, parent, false);
            v.SetBackgroundResource(mBackground);
            return new SymptomsViewHolder(v);
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var simpleHolder = holder as SymptomsViewHolder;
            if (MainActivity.LanguageSetting == "english")
            {
                simpleHolder.mTxtView.Text = s1List[position].title;
            }else if (MainActivity.LanguageSetting == "swahili")
            {
                simpleHolder.mTxtView.Text = s1List[position].title_sw;
            }
            try
            {
                string matchString = s1List[position].image_url;
                Picasso.With(mContext)
          .Load(matchString)
           .Fit()
           .CenterCrop()
          .Into(simpleHolder.mImageView);
            }
            catch (Exception ex)
            {
                Console.Write(ex);
            }

            simpleHolder.mCheckbox.Click += (o, e) => {
                if (simpleHolder.mCheckbox.Checked == true)
                {
                    myList.Add(s1List[position].id);
                    Console.Write("checked" + s1List[position].id);
                }
                else
                {

                    myList.RemoveAt(myList.Count - 1);
                    Console.Write("removed");

                }
               
            };



        }

        

      
    }



    public class SymptomsViewHolder : RecyclerView.ViewHolder
    {
        public readonly CardView cv;
        public readonly View mView;
        public readonly TextView mTxtView;
        public readonly ImageView mImageView;
        public readonly CheckBox mCheckbox;
       



        public SymptomsViewHolder(View view) : base(view)
        {

            cv = view.FindViewById<CardView>(Resource.Id.symptom_card);
            mView = view;

            mTxtView = view.FindViewById<TextView>(Resource.Id.symptom_list_title);
            // mImageView = view.FindViewById<ImageView>(Resource.Id.cardview_image);
            mImageView = view.FindViewById<ImageView>(Resource.Id.SymptomCardview_image);

             mCheckbox = view.FindViewById<CheckBox>(Resource.Id.symptom_checkbox);



        }
    }

}
