﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Support.V7.Widget;
using Android.Content;
using Android.Content.Res;
using Android.Util;
using Android.Views;
using Android.Widget;
using farm.Model;
using Square.Picasso;

namespace farm.Adapters
{
    public  class HomeRecyclerAdapter : RecyclerView.Adapter
    {

      
        private Context mContext;

        private readonly TypedValue mTypedValue = new TypedValue();
        Resources mResource;
        private int mBackground;
        public List<HomeLists> hList = new List<HomeLists>();
        private string language = MainActivity.LanguageSetting;
    


        public HomeRecyclerAdapter(Context context, List<HomeLists> hLists, Resources res)
        {
            context.Theme.ResolveAttribute(Resource.Attribute.selectableItemBackground, mTypedValue, true);
            mContext = context;

            mBackground = mTypedValue.ResourceId;
            hList = hLists;
            mResource = res;

        }



        public override int ItemCount
        {
            get
            {
                return hList.Count;
            }
        }





        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View v = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.home_item, parent, false);
            v.SetBackgroundResource(mBackground);
            return new PostViewHolder(v);
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var simpleHolder = holder as PostViewHolder;

            if (language == "english")
            {
                simpleHolder.mTxtView.Text = hList[position].title;
                simpleHolder.mTxtView1.Text = hList[position].description;
            }
            else if(language == "swahili")
            {
                simpleHolder.mTxtView.Text = hList[position].title_sw;
                simpleHolder.mTxtView1.Text = hList[position].description_sw;

            }
            else
            {
                simpleHolder.mTxtView.Text = hList[position].title;
                simpleHolder.mTxtView1.Text = hList[position].description;

            }
                try
            {
               
                string matchString = hList[position].image_url;
                Picasso.With(mContext)
          .Load(matchString)
           .Fit()
           .CenterCrop()
          .Into(simpleHolder.mImageView);
            }
            catch(Exception ex)
            {
                Console.Write(ex);
            }



        }


    }



    public class PostViewHolder : RecyclerView.ViewHolder
    {
        public readonly CardView cv;
        public readonly View mView;
        public readonly TextView mTxtView;
        public readonly ImageView mImageView;
        public readonly TextView mTxtView1;
       



        public PostViewHolder(View view) : base(view)
        {

            cv = view.FindViewById<CardView>(Resource.Id.home_card);
            mView = view;

            mTxtView = view.FindViewById<TextView>(Resource.Id.cardview_list_title);
            //mImageView = view.FindViewById<ImageView>(Resource.Id.cardview_image);

            mTxtView1 = view.FindViewById<TextView>(Resource.Id.short_description);

            mImageView = view.FindViewById<ImageView>(Resource.Id.cardview_image);
           



        }
    }
}