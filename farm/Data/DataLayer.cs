﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using farm.Model;

namespace farm.Data
{
    public class DataLayer
    {
        public static DataLayer Instance { get; } = new DataLayer();


        Database Database;

        public void SetDataBasePlatform()
        {
            if (Database == null)
            {
                Database = new Database();
                Database.Open();
            }
        }

        public async Task<List<HomeLists>> GetHomeListAsync()
        {
            var localPostList = new List<HomeLists>();

            try
            {
                SetDataBasePlatform();



                localPostList = Database.Fetch<HomeLists>().ToList();

                var remotePostsList = await WebApi.Instance.GetHomeListAsync();

                if(remotePostsList.Count() != 0 )
                {


                    localPostList = remotePostsList.ToList();
                    Database.TruncateTable<HomeLists>();
                    foreach (var post in localPostList)
                    {
                        Database.Insert(post);

                    }

                    return localPostList;

                }
             


            }
            catch (Exception ex)
            {
                //In case we have a problem...
                Console.WriteLine("Whooops! " + ex.Message);
            }

            return localPostList;
        }

        public async Task<List<Disease>> GetDiseaselistAsync()
        {
            var localPostList = new List<Disease>();

            try
            {
                SetDataBasePlatform();



                localPostList = Database.Fetch<Disease>().ToList();
                var remotePostsList = await WebApi.Instance.GetDiseaseListAsync();

                if (remotePostsList.Count() != 0)
                {


                    localPostList = remotePostsList.ToList();
                    Database.TruncateTable<Disease>();
                    foreach (var post in localPostList)
                    {
                        Database.Insert(post);

                    }

                    return localPostList;

                }


            }
            catch (Exception ex)
            {
                //In case we have a problem...
                Console.WriteLine("Whooops! " + ex.Message);
            }

            return localPostList;
        }

        public async Task<List<Symptoms>> GetSymptomlistAsync()
        {
            var localPostList = new List<Symptoms>();

            try
            {
                SetDataBasePlatform();

                localPostList = Database.Fetch<Symptoms>().ToList();

                var remotePostsList = await WebApi.Instance.GetSymptomListAsync();

                if (remotePostsList.Count() != 0)
                {


                    localPostList = remotePostsList.ToList();
                    Database.TruncateTable<Symptoms>();
                    foreach (var post in localPostList)
                    {
                        Database.Insert(post);

                    }

                    return localPostList;

                }
            }
            catch (Exception ex)
            {
                //In case we have a problem...
                Console.WriteLine("Whooops! " + ex.Message);
            }

            return localPostList;
        }

        public async Task<List<Advice>> GetAdviceListAsync()
        {
            var localPostList = new List<Advice>();

            try
            {
                SetDataBasePlatform();

                localPostList = Database.Fetch<Advice>().ToList();

                var remotePostsList = await WebApi.Instance.GetAdviceListAsync();

                if (remotePostsList.Count() != null)
                {


                    localPostList = remotePostsList.ToList();
                    Database.TruncateTable<Advice>();
                    foreach (var post in localPostList)
                    {
                        Database.Insert(post);

                    }

                    return localPostList;

                }
            }
            catch (Exception ex)
            {
                //In case we have a problem...
                Console.WriteLine("Whooops! " + ex.Message);
            }

            return localPostList;
        }

        public async Task<List<Prevention>> GetPreventionListAsync()
        {
            var localPostList = new List<Prevention>();

            try
            {
                SetDataBasePlatform();

                localPostList = Database.Fetch<Prevention>().ToList();

                var remotePostsList = await WebApi.Instance.GetPreventionListAsync();

                if (remotePostsList.Count() != 0)
                {


                    localPostList = remotePostsList.ToList();
                    Database.TruncateTable<Prevention>();
                    foreach (var post in localPostList)
                    {
                        Database.Insert(post);

                    }

                    return localPostList;

                }
            }
            catch (Exception ex)
            {
                //In case we have a problem...
                Console.WriteLine("Whooops! " + ex.Message);
            }

            return localPostList;
        }

        public async Task<List<Treatment>> GetTreatmentListAsync()
        {
            var localPostList = new List<Treatment>();

            try
            {
                SetDataBasePlatform();

                localPostList = Database.Fetch<Treatment>().ToList();

                var remotePostsList = await WebApi.Instance.GetTreatmentListAsync();

                if (remotePostsList.Count() != 0)
                {


                    localPostList = remotePostsList.ToList();
                    Database.TruncateTable<Treatment>();
                    foreach (var post in localPostList)
                    {
                        Database.Insert(post);

                    }

                    return localPostList;

                }
            }
            catch (Exception ex)
            {
                //In case we have a problem...
                Console.WriteLine("Whooops! " + ex.Message);
            }

            return localPostList;
        }


    }
}