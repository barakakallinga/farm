﻿using farm.Model;
using Newtonsoft.Json;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace farm.Data
{
    class WebApi
    {

        //Static instace so we can acces this class everywhere
        public static WebApi Instance { get; } = new WebApi();

        //Our base API url
        static string _baseUrl { get { return "http://farm.pbmtechy.com/index.php"; } }

        HttpClient CreateClient()
        {
            var httpCLient = new HttpClient()
            {
                BaseAddress = new Uri(_baseUrl)
            };

            return httpCLient;
        }

        public async Task<List<HomeLists>> GetHomeListAsync()
        {
            var hLists = new List<HomeLists>();
            try
            {
                //TODO: Check network connection
                if (await CheckNetworkConnection())
                {
                    var url = "/api/stages";
                    using (var httpClient = CreateClient())
                    {
                        var result = await httpClient.GetAsync(url);
                        var responseText = await result.Content.ReadAsStringAsync();
                        //Serialize the json object to our c# classes
                        var rootObject = JsonConvert.DeserializeObject<List<HomeLists>>(responseText);

                        foreach (var hList in rootObject)
                        {
                            hLists.Add(new HomeLists
                            {
                                id = hList.id,
                                title = hList.title,
                                title_sw = hList.title_sw,
                                description = hList.description,
                                description_sw = hList.description_sw,
                                image_url = hList.image_url,

                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //In case we have a problem...
                Console.WriteLine("Whooops! " + ex.Message);
            }
            return hLists;
        }

        public async Task<List<Disease>> GetDiseaseListAsync()
        {
            var hLists = new List<Disease>();
            try
            {
                //TODO: Check network connection
                if (await CheckNetworkConnection())
                {
                    var url = "/api/disease";
                    using (var httpClient = CreateClient())
                    {
                        var result = await httpClient.GetAsync(url);
                        var responseText = await result.Content.ReadAsStringAsync();
                        //Serialize the json object to our c# classes
                        var rootObject = JsonConvert.DeserializeObject<List<Disease>>(responseText);

                        foreach (var hList in rootObject)
                        {
                            hLists.Add(new Disease
                            {
                                id = hList.id,
                                title = hList.title,
                                title_sw = hList.title_sw,
                                description = hList.description,
                                description_sw = hList.description_sw,


                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //In case we have a problem...
                Console.WriteLine("Whooops! " + ex.Message);
            }
            return hLists;
        }

        public async Task<List<Symptoms>> GetSymptomListAsync()
        {
            var hLists = new List<Symptoms>();
            try
            {
                //TODO: Check network connection
                if (await CheckNetworkConnection())
                {
                    var url = "/api/symptoms";
                    using (var httpClient = CreateClient())
                    {
                        var result = await httpClient.GetAsync(url);
                        var responseText = await result.Content.ReadAsStringAsync();
                        //Serialize the json object to our c# classes
                        var rootObject = JsonConvert.DeserializeObject<List<Symptoms>>(responseText);

                        foreach (var hList in rootObject)
                        {
                            hLists.Add(new Symptoms
                            {
                                id = hList.id,
                                title = hList.title,
                                title_sw = hList.title_sw,
                                image_url = hList.image_url,
                                stage_id = hList.stage_id,
                                disease_id = hList.disease_id


                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //In case we have a problem...
                Console.WriteLine("Whooops! " + ex.Message);
            }
            return hLists;
        }

        public async Task<List<Advice>> GetAdviceListAsync()
        {
            var hLists = new List<Advice>();
            try
            {
                //TODO: Check network connection
                if (await CheckNetworkConnection())
                {
                    var url = "/api/advice";
                    using (var httpClient = CreateClient())
                    {
                        var result = await httpClient.GetAsync(url);
                        var responseText = await result.Content.ReadAsStringAsync();
                        //Serialize the json object to our c# classes
                        var rootObject = JsonConvert.DeserializeObject<List<Advice>>(responseText);

                        foreach (var hList in rootObject)
                        {
                            hLists.Add(new Advice
                            {
                                id = hList.id,
                                advice = hList.advice,
                                advice_sw = hList.advice_sw,
                                disease_id = hList.disease_id

                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //In case we have a problem...
                Console.WriteLine("Whooops! " + ex.Message);
            }
            return hLists;
        }

        public async Task<List<Prevention>> GetPreventionListAsync()
        {
            var hLists = new List<Prevention>();
            try
            {
                //TODO: Check network connection
                if (await CheckNetworkConnection())
                {
                    var url = "/api/prevention";
                    using (var httpClient = CreateClient())
                    {
                        var result = await httpClient.GetAsync(url);
                        var responseText = await result.Content.ReadAsStringAsync();
                        //Serialize the json object to our c# classes
                        var rootObject = JsonConvert.DeserializeObject<List<Prevention>>(responseText);

                        foreach (var hList in rootObject)
                        {
                            hLists.Add(new Prevention
                            {
                                id = hList.id,
                                disease_id = hList.disease_id,
                                prevention = hList.prevention,
                                prevention_sw = hList.prevention_sw

                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //In case we have a problem...
                Console.WriteLine("Whooops! " + ex.Message);
            }
            return hLists;
        }

        public async Task<List<Treatment>> GetTreatmentListAsync()
        {
            var hLists = new List<Treatment>();
            try
            {
                //TODO: Check network connection
                if (await CheckNetworkConnection())
                {
                    var url = "/api/treatment";
                    using (var httpClient = CreateClient())
                    {
                        var result = await httpClient.GetAsync(url);
                        var responseText = await result.Content.ReadAsStringAsync();
                        //Serialize the json object to our c# classes
                        var rootObject = JsonConvert.DeserializeObject<List<Treatment>>(responseText);

                        foreach (var hList in rootObject)
                        {
                            hLists.Add(new Treatment
                            {
                                id = hList.id,
                                disease_id = hList.disease_id,
                                description = hList.description,
                                description_sw = hList.description_sw

                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //In case we have a problem...
                Console.WriteLine("Whooops! " + ex.Message);
            }
            return hLists;
        }












        public static async Task<bool> CheckNetworkConnection()
        {
            if (!CrossConnectivity.Current.IsConnected)
            {
                return false;
            }

            if (!await CrossConnectivity.Current.IsRemoteReachable("https://www.google.com"))
            {
                return false;
            }

            return true;
        }


    }
}