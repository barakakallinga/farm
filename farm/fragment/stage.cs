﻿using Android.App;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using farm.Adapters;
using farm.Data;
using farm.Model;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace farm.fragment
{
    public class stage : Android.Support.V4.App.Fragment
    {
        private RecyclerView stage1recyclerview;
        private List<Symptoms> hLists;
        private string stage_id;
     


        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            //ProgressDialog progress = new ProgressDialog(this.Context);
            //progress.SetMessage("Loading...");
            //progress.Show();
            //progress.Dismiss();

            View view = inflater.Inflate(Resource.Layout.stage1, container, false);

            if (MainActivity.LanguageSetting == "swahili")
            {
                TextView text = view.FindViewById<TextView>(Resource.Id.symptom_list_Heading);

                text.Text = "Chagua Dalili";
            }
            else if (MainActivity.LanguageSetting == "english")
            {

                TextView text = view.FindViewById<TextView>(Resource.Id.symptom_list_Heading);

                text.Text = "Select Symptom";



            }

            stage1recyclerview = view.FindViewById<RecyclerView>(Resource.Id.recyclerviewStage1);

            SetUpRecyclerView(stage1recyclerview);

            FloatingActionButton Fab = view.FindViewById<FloatingActionButton>(Resource.Id.fab);


            Fab.Click += (o, e) => {

                var s = new SymptomsRecyclerAdapter(stage1recyclerview.Context, hLists, Resources);

                //var maxRepeatedItem = s.GetList().GroupBy(x => x)
                //          .OrderByDescending(x => x.Count())
                //          .First().Key;

                var grouped = s.GetList().ToLookup(x => x);
                if (grouped.Count() != 0)
                {
                    var maxRepetitions = grouped.Max(x => x.Count());
                    var maxRepeatedItems = grouped.Where(x => x.Count() == maxRepetitions)
                                                  .Select(x => x.Key).ToList();


                    Android.Support.V4.App.FragmentTransaction transaction1 = FragmentManager.BeginTransaction();

                    treatment one = new treatment();
                    Bundle args = new Bundle();
                    args.PutStringArrayList("symptom_id", maxRepeatedItems);
                    one.Arguments = args;
                    transaction1.Replace(Resource.Id.container, one);
                    transaction1.AddToBackStack(null);
                    transaction1.Commit();
                    s.ClerList();

                }



            };


            return view;
        }

        private void SetUpRecyclerView(RecyclerView recyclerView)
        {

            recyclerView.SetLayoutManager(new LinearLayoutManager(recyclerView.Context));

            if (hLists == null)
            {
                stage_id = Arguments.GetString("stage_id");

              
                    var t = Task.Run(async () =>
                    {

                        var ordered = await DataLayer.Instance.GetSymptomlistAsync();

                        hLists = ordered.Where(x => x.stage_id == stage_id).ToList();

                    });

                    t.Wait();
                
            }


            recyclerView.SetAdapter(new SymptomsRecyclerAdapter(recyclerView.Context, hLists, Resources));






        }

       


    }
}