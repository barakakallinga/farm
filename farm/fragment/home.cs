﻿using Android.App;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using farm.Adapters;
using farm.Data;
using farm.Model;
using muakilishi.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace farm.fragment
{
    public class home : Android.Support.V4.App.Fragment
    {

        private RecyclerView homerecyclerview;
        private List<HomeLists> hLists = new List<HomeLists>();


        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.fragment1, container, false);

            homerecyclerview = view.FindViewById<RecyclerView>(Resource.Id.recyclerview_mainpage);

            //if(MainActivity.LanguageSetting == "swahili")
            //{
            //    TextView text = view.FindViewById<TextView>(Resource.Id.welcome_heading);

            //    text.Text = "Chagua Hatua";
            //}
            //else if(MainActivity.LanguageSetting == "english")
            //{

            //    TextView text = view.FindViewById<TextView>(Resource.Id.welcome_heading);

            //    text.Text = "Select Phase";



            //}

            SetUpRecyclerView(homerecyclerview);



            return view;
        }


        private void SetUpRecyclerView(RecyclerView recyclerView)
        {

            recyclerView.SetLayoutManager(new LinearLayoutManager(recyclerView.Context));

            if (hLists.Count == 0)
            {
                var t = Task.Run(async () =>
                {

                   var ordered = await DataLayer.Instance.GetHomeListAsync();

                    hLists = ordered.ToList();
                    
                });

                t.Wait();


            }


            recyclerView.SetAdapter(new HomeRecyclerAdapter(recyclerView.Context, hLists, Resources));



            recyclerView.SetItemClickListener((rv, position, view) =>
            {
                //ProgressDialog progress = new ProgressDialog(this.Context);
                //progress.SetMessage("Loading...");
                //progress.Show();
                Android.Support.V4.App.FragmentTransaction transaction1 = FragmentManager.BeginTransaction();

                stage one = new stage();
                Bundle args = new Bundle();
                args.PutString("stage_id", hLists[position].id);
                one.Arguments = args;
                transaction1.Replace(Resource.Id.container, one);
                transaction1.AddToBackStack(null);
                transaction1.Commit();


                ////An item has been clicked
                //Context context = view.Context;
                //Intent intent = new Intent(context, typeof(PostDetailActivity));
                //intent.PutExtra(PostDetailActivity.POST_ID, posts[position].postId);
                //intent.PutExtra(PostDetailActivity.POST_TITLE, posts[position].postTitle);


                //context.StartActivity(intent);
            });



        }


    }
}