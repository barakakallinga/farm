﻿using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using farm.Adapters;
using farm.Data;
using farm.Model;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace farm.fragment
{
    public class treatment : Android.Support.V4.App.Fragment
    {
        private RecyclerView treatmentRecyclerview;
        private List<Treatment> tLists;
        private List<Treatment> fulltLists;
        private List<Disease> dList;
        private List<string> diseaseIDList;
        private string symptom_id;
        private Button advice;
        private Button prevention;
        
        private List<string> symptom_list;
        private List<combinedList> combList;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            View view = inflater.Inflate(Resource.Layout.treatment, container, false);

            

            var t = Task.Run(async () =>
            {
                dList = await DataLayer.Instance.GetDiseaselistAsync();

            });

            t.Wait();


            treatmentRecyclerview = view.FindViewById<RecyclerView>(Resource.Id.recyclerviewTreatment);

            SetUpRecyclerView(treatmentRecyclerview);

            advice = view.FindViewById<Button>(Resource.Id.button_advice);

            advice.Click += Advice_Click;
            
            prevention = view.FindViewById<Button>(Resource.Id.button_prevention);

            prevention.Click += Prevention_Click;




            return view;
        }

        private void Prevention_Click(object sender, System.EventArgs e)
        {

            Android.Support.V4.App.FragmentTransaction transaction1 = FragmentManager.BeginTransaction();

            prevention one = new prevention();
            Bundle args = new Bundle();
            args.PutStringArrayList("disease_id", diseaseIDList);
            one.Arguments = args;
            transaction1.Replace(Resource.Id.container, one);
            transaction1.AddToBackStack(null);
            transaction1.Commit();

        }

        private void Advice_Click(object sender, System.EventArgs e)
        {
            Android.Support.V4.App.FragmentTransaction transaction1 = FragmentManager.BeginTransaction();


           

            advice one = new advice();
            Bundle args = new Bundle();
            args.PutStringArrayList("disease_id", diseaseIDList);
            one.Arguments = args;
            transaction1.Replace(Resource.Id.container, one);
            transaction1.AddToBackStack(null);
            transaction1.Commit();
         

        }

        private void SetUpRecyclerView(RecyclerView recyclerView)
        {

            recyclerView.SetLayoutManager(new LinearLayoutManager(recyclerView.Context));

            symptom_list = Arguments.GetStringArrayList("symptom_id").ToList();
           

            symptom_id = Arguments.GetString("symptom_id");
            combList = new List<combinedList>();

            var diseaseids = new List<Symptoms>();

            //var tx = Task.Run(async () =>
            //{

            //    var ordered = await DataLayer.Instance.GetSymptomlistAsync();
              

            //    diseaseids = ordered.Where(x => symptom_list.Contains(x.id)).Select(x => x.).ToList();

            //});

            //t.Wait();




            var t = Task.Run(async () =>
                {

                    var ordered = await DataLayer.Instance.GetTreatmentListAsync();
                    fulltLists = ordered;

                    tLists = ordered.Where(x => symptom_list.Contains(x.disease_id )).ToList();

                });

                t.Wait();











            var diseaseLList = tLists.Select(x => x.disease_id).Distinct();
            diseaseIDList = diseaseLList.ToList();

            foreach (var dl in diseaseLList)
            {

                var dtitle = dList.Where(x => x.id == dl).Select(x => x.title).FirstOrDefault();

                combList.Add(new combinedList
                {
                   
                    Disease = dtitle,
                    treatment = "",
                    mType = combinedList.HEADER,

                });

                var groupItem = fulltLists.Where(x => x.disease_id == dl).Select(x => x.description_sw);

                foreach(var gi in groupItem)
                {

                    combList.Add(new combinedList
                    {

                        Disease = "",
                        treatment = gi,
                        mType = combinedList.CONTENT,

                    });




                }


            }

            recyclerView.SetAdapter(new TreatmentRecyclerAdapter(recyclerView.Context, combList, Resources));


        }


    }
}