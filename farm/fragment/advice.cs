﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Util;
using Android.Views;
using Android.Widget;
using farm.Adapters;
using farm.Data;
using farm.Model;

namespace farm.fragment
{
    public class advice : Android.Support.V4.App.Fragment
    {

        private List<Disease> dList;
        private RecyclerView adviceRecyclerview;
        private List<string> symptom_list;
     
        private List<AdviceCombinedList> combList;
        private List<Advice> fulltLists;
        private List<Advice> aLists;
        private Button prevention;
        private List<string> diseaseIDList;




        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);           
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

           View view = inflater.Inflate(Resource.Layout.advice, container, false);



            var t = Task.Run(async () =>
            {
                dList = await DataLayer.Instance.GetDiseaselistAsync();

            });

            t.Wait();


            adviceRecyclerview = view.FindViewById<RecyclerView>(Resource.Id.recyclerviewTreatment_advice);

            SetUpRecyclerView(adviceRecyclerview);

            prevention = view.FindViewById<Button>(Resource.Id.button_prevention_advice);

            prevention.Click += Prevention_Click;





            return view;

        }

        private void Prevention_Click(object sender, System.EventArgs e)
        {

            Android.Support.V4.App.FragmentTransaction transaction1 = FragmentManager.BeginTransaction();

            prevention one = new prevention();
            Bundle args = new Bundle();
            args.PutStringArrayList("disease_id", diseaseIDList);
            one.Arguments = args;
            transaction1.Replace(Resource.Id.container, one);
            transaction1.AddToBackStack(null);
            transaction1.Commit();

        }

        private void SetUpRecyclerView(RecyclerView recyclerView)
        {

            recyclerView.SetLayoutManager(new LinearLayoutManager(recyclerView.Context));

            symptom_list = Arguments.GetStringArrayList("disease_id").ToList();

         
            diseaseIDList = symptom_list;

            combList = new List<AdviceCombinedList>();


            var t = Task.Run(async () =>
            {

                var ordered = await DataLayer.Instance.GetAdviceListAsync();
                fulltLists = ordered;

                aLists = ordered.Where(x => symptom_list.Contains(x.disease_id)).ToList();

            });

            t.Wait();

            var diseaseLList = aLists.Select(x => x.disease_id).Distinct();

            foreach (var dl in diseaseLList)
            {

                var dtitle = dList.Where(x => x.id == dl).Select(x => x.title).FirstOrDefault();

                combList.Add(new AdviceCombinedList
                {
                    
                    Disease = dtitle,
                    advice = "",
                    mType = AdviceCombinedList.ADVICEHEADER,

                });

                var groupItem = fulltLists.Where(x => x.disease_id == dl).Select(x => x.advice_sw);

                foreach (var gi in groupItem)
                {

                    combList.Add(new AdviceCombinedList
                    {

                        Disease = "",
                        advice = gi,
                        mType = AdviceCombinedList.ADVICECONTENT,

                    });




                }


            }

            recyclerView.SetAdapter(new AdviceRecyclerAdapter(recyclerView.Context, combList, Resources));


        }
    }
}