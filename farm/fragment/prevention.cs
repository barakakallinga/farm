﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using farm.Adapters;
using farm.Data;
using farm.Model;

namespace farm.fragment
{
   public  class prevention : Android.Support.V4.App.Fragment
    {


        private List<Disease> dList;
        private RecyclerView preventionRecyclerview;
        private List<string> disease_list;
        private string symptom_id;
        private List<PreventionCombinedList> combList;
        private List<Advice> fulltLists;
        private List<Advice> aLists;




        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            View view = inflater.Inflate(Resource.Layout.prevention, container, false);



            var t = Task.Run(async () =>
            {
                dList = await DataLayer.Instance.GetDiseaselistAsync();

            });

            t.Wait();


            preventionRecyclerview = view.FindViewById<RecyclerView>(Resource.Id.recyclerviewPrevention_advice);

            SetUpRecyclerView(preventionRecyclerview);

            return view;

        }



        private void SetUpRecyclerView(RecyclerView recyclerView)
        {

            recyclerView.SetLayoutManager(new LinearLayoutManager(recyclerView.Context));

            disease_list = Arguments.GetStringArrayList("disease_id").ToList();


            combList = new List<PreventionCombinedList>();


            var t = Task.Run(async () =>
            {

                var ordered = await DataLayer.Instance.GetAdviceListAsync();
                fulltLists = ordered;

                aLists = ordered.Where(x => disease_list.Contains(x.disease_id)).ToList();

            });

            t.Wait();

            var diseaseLList = aLists.Select(x => x.disease_id).Distinct();

            foreach (var dl in diseaseLList)
            {

                var dtitle = dList.Where(x => x.id == dl).Select(x => x.title).FirstOrDefault();

                combList.Add(new PreventionCombinedList
                {

                    Disease = dtitle,
                    prevention = "",
                    mType = PreventionCombinedList.PREVENTIONHEADER,

                });

                var groupItem = fulltLists.Where(x => x.disease_id == dl).Select(x => x.advice_sw);

                foreach (var gi in groupItem)
                {

                    combList.Add(new PreventionCombinedList
                    {

                        Disease = "",
                        prevention = gi,
                        mType = PreventionCombinedList.PREVENTIONCONTENT,

                    });




                }


            }

            recyclerView.SetAdapter(new PreventionRecyclerAdapter(recyclerView.Context, combList, Resources));


        }



    }
}