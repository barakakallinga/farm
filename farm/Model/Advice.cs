﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace farm.Model
{
    public class Advice : BaseModel
    {
        public string id { get; set; }
        public string advice { get; set; }

        public string advice_sw { get; set; }

        public string disease_id { get; set; }

    }
}