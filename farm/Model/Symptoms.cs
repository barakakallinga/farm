﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace farm.Model
{
    public class Symptoms : BaseModel
    {

        public string id { get; set; }
        public string title { get; set; }

        public string title_sw { get; set; }
        public string image_url { get; set; }
        public string disease_id {get; set;}
        public string stage_id { get; set; }


    }
}