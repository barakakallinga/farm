﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

using SQLite.Net.Attributes;

namespace farm.Model
{
    public class BaseModel
    {

        [PrimaryKey, AutoIncrement, JsonIgnore]
        public int DefaultId { get; set; }

        [Default(true), JsonIgnore]
        public DateTime Created { get; set; } = DateTime.UtcNow;

        [Default(true), JsonIgnore]
        public DateTime Updated { get; set; } = DateTime.UtcNow;


        public BaseModel()
        {

        }

    }
}